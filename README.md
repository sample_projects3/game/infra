#  requirements

## terraform (v1.7.4) <https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli>
## yc cli (v0.120.0) <https://cloud.yandex.com/en-ru/docs/cli/quickstart>
## kubectl (1.26.2) <https://kubernetes.io/docs/tasks/tools>
## aws cli (2.15.38) <https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html> — for creating ydb terraform remote state locking table

```console
alias tf=terraform
alias k=kubectl
```

```console
.
├── helm — helm-charts for system/initial apps
│   └── ingress-nginx
├── kubectl_manifests — manifest applied by kubectl cli
└── terraform
    ├── default — tf manifests for resources in «default» yandex cloud folder
    ├── dev — tf manifests for resources in «dev» yandex cloud folder
    └── s3_backend — manifests for tf remote state in s3 bucket
```

`git rev-parse --show-toplevel` — returns root dir of a git repo

### setup terraform s3 bucket for remote state
```console
bash $(git rev-parse --show-toplevel)/terraform/s3_backend/tf_backend.sh
```
### add generated terraform creds(terraform/credentials.encrypted, terraform/credentials.json.encrypted) to git repo

### create «dev» yandex cloud folder
```console
cd $(git rev-parse --show-toplevel)/terraform/dev/folder && \
tf init -backend-config=$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend -backend-config=s3_backend.tfbackend && \
tf apply
```

### create vpc and public ip for network load balancer
```console
cd $(git rev-parse --show-toplevel)/terraform/dev/vpc && \
tf init -backend-config=$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend -backend-config=s3_backend.tfbackend && \
tf apply
```

### create k8s cluster
```console
cd $(git rev-parse --show-toplevel)/terraform/dev/k8s && \
tf init -backend-config=$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend -backend-config=s3_backend.tfbackend && \
tf apply
```

### generate kubeconfig
```console
yc managed-kubernetes cluster get-credentials $(cd $(git rev-parse --show-toplevel)/terraform/dev/k8s && tf output -raw cluster_id) --external --kubeconfig ~/.kube/config_game_app_dev
```
### save path to kubeconfig in env var
```console
export KUBECONFIG=~/.kube/config_game_app_dev
```
### create k8s namespaces
```console
k apply -f $(git rev-parse --show-toplevel)/kubectl_manifests/namespaces.yml
```

### install cert manager
```console
k apply -f $(git rev-parse --show-toplevel)/kubectl_manifests/cert-manager.yaml
```

### create service acc for terraform
```console
k apply -f $(git rev-parse --show-toplevel)/kubectl_manifests/terraform-service-acc.yml
```

### save token for terraform acc in yandex lockbox (name of the lockbox secret should be uniq)
```console
yc lockbox secret create \
  --name k8s-terraform-acc-token \
  --description "token for terraform service acc in k8s" \
  --payload "[{'key': 'token', 'text_value': '"$(k -nsystem get secret terraform-token -o jsonpath="{.data.token}"|base64 -d)"'}]"  \
  --deletion-protection \
  --folder-name dev
```

### apply manifests for nginx-ingress, cicd service acc and other resources
```console
cd $(git rev-parse --show-toplevel)/terraform/dev/k8s_manifests && \
tf init -backend-config=$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend -backend-config=s3_backend.tfbackend && \
tf apply
```


## add variables for pipeline in Gitlab
 - CONTAINER_REGISTRY_ENDPOINT — cr.yandex in cr.yandex/123…abc/namespace-path/image:tag
 - CONTAINER_REGISTRY_PROJECT — 123…abc in cr.yandex/123…abc/namespace-path/image:tag `cd $(git rev-parse --show-toplevel)/terraform/default/container_registry && tf output -raw container_registry_id`

secret/masked vars
 - YANDEX_SERVICE_ACC_KEY_JSON_BASE64 — IAM key for yandex-cloud cicd service acc in base64 format (cd $(git rev-parse --show-toplevel)/terraform/default/container_registry; tf output -raw cicd_auth_key)

environment specific vars
 - K8S_API_SERVER_URL — `k cluster-info |head -1 |grep -oP '(?<=is running at ).*?://\d*\.\d*\.\d*\.\d*:*\d*'` or `cd $(git rev-parse --show-toplevel)/terraform/dev/k8s && tf output -raw k8s_endpoint`
 - K8S_NAMESPACE

environment specific secret/masked vars
 - K8S_CICD_TOKEN_BASE64 — token of cicd service acc in k8s `k -ndev get secret ci-cd-token -o jsonpath="{.data.token}"`

## commands for decrypting terraform creds
```console
yc kms symmetric-crypto decrypt \
  --id $(yc kms symmetric-key get tf-creds-symmetric-key --folder-name default | grep -oP "(?<=^id: ).*") \
  --plaintext-file $(git rev-parse --show-toplevel)/terraform/credentials.json \
  --ciphertext-file $(git rev-parse --show-toplevel)/terraform/credentials.json.encrypted
yc kms symmetric-crypto decrypt \
  --id $(yc kms symmetric-key get tf-creds-symmetric-key --folder-name default | grep -oP "(?<=^id: ).*") \
  --plaintext-file $(git rev-parse --show-toplevel)/terraform/credentials \
  --ciphertext-file $(git rev-parse --show-toplevel)/terraform/credentials.encrypted
```

# Possible improvments
 - move helm chart from app repo to a separate repo
 - consider deploying applicaions with Argo CD
 - add monitoring/logging tools to k8s
 - add lint/test stage to GitLab CI
 - add pipeline steps for prod environment
 - make diagram/visualization of infrastructure
