resource "yandex_container_registry" "default" {
  name      = var.registry_name
  folder_id = data.yandex_resourcemanager_folder.this.id

  lifecycle {
    prevent_destroy = true
  }
}
