resource "yandex_iam_service_account" "cicd" {
  folder_id   = data.yandex_resourcemanager_folder.this.id
  name        = var.cicd_service_acc_name
  description = "The service account used by cicd pipelines to push and pull images from container registry"
}


resource "yandex_resourcemanager_folder_iam_member" "cicd_images_pusher" {
  folder_id = data.yandex_resourcemanager_folder.this.id

  role   = "container-registry.images.pusher"
  member = "serviceAccount:${yandex_iam_service_account.cicd.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "cicd_images_puller" {
  folder_id = data.yandex_resourcemanager_folder.this.id

  role   = "container-registry.images.puller"
  member = "serviceAccount:${yandex_iam_service_account.cicd.id}"
}


resource "yandex_iam_service_account_key" "cicd_auth_key" {
  service_account_id = yandex_iam_service_account.cicd.id
  description        = "for cicd runners"
  key_algorithm      = "RSA_4096"
}

locals {
  filtered_cicd_auth_key_base64 = base64encode(jsonencode(merge(
    {
      for key, value in yandex_iam_service_account_key.cicd_auth_key :
      key => value
      if key != "pgp_key" && key != "key_fingerprint" && key != "format" && key != "encrypted_private_key"
    }
  )))
}
