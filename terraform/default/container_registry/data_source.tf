data "yandex_resourcemanager_cloud" "this" {
  name = var.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  name     = var.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.this.id
}
