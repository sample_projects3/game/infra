output "cicd_auth_key" {
  description = "IAM key which content should be save to gitlab ci variable in base64 format"
  sensitive   = true
  value       = local.filtered_cicd_auth_key_base64
}

output "container_registry_id" {
  description = "Container registry id, should be set as a value for CONTAINER_REGISTRY_PROJECT Gitlab ci variable"
  value       = yandex_container_registry.default.id
}
