variable "cloud_name" {
  type = string
}

variable "folder_name" {
  type    = string
  default = "default"
}

variable "registry_name" {
  description = "must match pattern /|[a-z][-a-z0-9]{1,61}[a-z0-9]/"
  type        = string
}

variable "cicd_service_acc_name" {
  description = "The service account used by cicd pipelines to push and pull images from container registry"
  type        = string
  default     = "cicd"
}
