resource "yandex_resourcemanager_folder" "this" {
  cloud_id = data.yandex_resourcemanager_cloud.this.id
  name     = var.folder_name
}
