# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.6.0"
  constraints = "> 3.3.0"
  hashes = [
    "h1:R5Ucn26riKIEijcsiOMBR3uOAjuOMfI1x7XvH4P6B1w=",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version     = "0.11.1"
  constraints = "> 0.9.0"
  hashes = [
    "h1:IkDriv5C9G+kQQ+mP+8QGIahwKgbQcw1/mzh9U6q+ZI=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.113.0"
  constraints = "> 0.8.0, 0.113.0"
  hashes = [
    "h1:du6IqLZwZWJ0cD8KJE+2BXKyxPzlySDIbagX9wzYtlo=",
  ]
}
