module "k8s" {
  source = "github.com/terraform-yc-modules/terraform-yc-kubernetes.git?ref=1404ba83fc47c36cd6e8144bf1ab91589e5e78b3"

  network_id = data.terraform_remote_state.vpc.outputs.network_id
  folder_id  = data.yandex_resourcemanager_folder.this.id

  public_access = var.cluster_public_access

  cluster_name = var.cluster_name

  cluster_version = var.cluster_version

  master_locations = [
    {
      zone      = "ru-central1-d"
      subnet_id = data.terraform_remote_state.vpc.outputs.private_subnets["10.130.0.0/24"].subnet_id
    }
  ]

  master_maintenance_windows = [
    {
      day        = "monday"
      start_time = "20:00"
      duration   = "1h"
    }
  ]

  node_groups_defaults = var.node_groups_defaults

  master_labels = {
    role    = "master"
    service = "kubernetes"
  }

  master_logging = {
    enabled                = false
    folder_id              = null
    enabled_kube_apiserver = true
    enabled_autoscaler     = true
    enabled_events         = true
    enabled_audit          = false
  }

  node_groups = {
    "yc-k8s-ng-01" = {
      description = "Kubernetes nodes group 01 with auto scaling"
      auto_scale = {
        min     = 2
        max     = 4
        initial = 2
      }
      # only one location allowed for auto_scale group
      node_locations = [
        {
          zone      = "ru-central1-d"
          subnet_id = data.terraform_remote_state.vpc.outputs.private_subnets["10.130.0.0/24"].subnet_id
        }
      ]
      labels = {
        service = "kubernetes"
      }
      node_labels = {
        role        = "worker-01"
        environment = "dev"
      }
    },
  }
}
