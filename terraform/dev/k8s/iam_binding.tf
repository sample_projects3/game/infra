resource "yandex_resourcemanager_folder_iam_member" "k8s_image_puller" {
  folder_id = data.yandex_resourcemanager_folder.container_registry.id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${module.k8s.node_account_id}"
}
