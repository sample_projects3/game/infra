provider "yandex" {
  service_account_key_file = "./../../credentials.json"
}

terraform {
  backend "s3" {}
}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket                   = "n98gt-tf-state-game"
    key                      = "game-project/yandex/folders/${var.folder_name}/vpc/terraform.tfstate"
    region                   = "us-east-1"
    profile                  = "tf-state-acc"
    shared_credentials_files = ["./../../credentials"]
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true # required for tf v1.6.1+
  }
}
