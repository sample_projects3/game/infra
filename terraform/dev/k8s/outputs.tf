output "k8s_endpoint" {
  value = module.k8s.external_v4_endpoint
}

output "cluster_id" {
  value = module.k8s.cluster_id
}
