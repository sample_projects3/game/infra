cloud_name  = "company"
folder_name = "dev"

cluster_public_access = true
cluster_version       = "1.28"

node_groups_defaults = {
  platform_id   = "standard-v3"
  node_cores    = 2 # vcpu
  node_memory   = 4 # ram gb
  node_gpus     = 0
  core_fraction = 50
  disk_type     = "network-ssd"
  disk_size     = 30 # Node disk size must be greater or equal to 16.0 GB
  preemptible   = false
  nat           = false
  ipv4          = true
  ipv6          = false
}
