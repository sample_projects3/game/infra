variable "cluster_name" {
  type    = string
  default = "k8s-cluster"
}

variable "cloud_name" {
  type = string
}

variable "folder_name" {
  type = string
}

variable "node_groups_defaults" {
  description = "A map of common default values for Node groups."
  type        = map(any)
  default = {
    platform_id   = "standard-v3"
    node_cores    = 2 # vcpu
    node_memory   = 4 # ram gb
    node_gpus     = 0
    core_fraction = 50            # Baseline core performance as a percentage (20,50,100)
    disk_type     = "network-ssd" # |network-ssd|network-nvme|network-hdd|network-ssd-nonreplicated
    disk_size     = 30
    preemptible   = false
    nat           = true
    ipv4          = true
    ipv6          = false
  }
  validation {
    condition     = var.node_groups_defaults.disk_size >= 30
    error_message = "Disk size must be equal to or greater than 30.0 GB"
  }
}


variable "container_registry_folder_name" {
  description = "Name of folder where container registry with images for k8s applications is located"
  type        = string
  default     = "default"
}


variable "cluster_public_access" {
  description = "Public or private Kubernetes cluster"
  type        = bool
  default     = true
}

variable "cluster_version" {
  description = "Kubernetes cluster version"
  type        = string
  default     = "1.28"
}
