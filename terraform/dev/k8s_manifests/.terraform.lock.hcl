# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.13.0"
  constraints = "2.13.0"
  hashes = [
    "h1:hQIozFWLJk67bKslnNIZPHYb037+6uO86sHVkntPjXY=",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.27.0"
  constraints = "2.27.0"
  hashes = [
    "h1:TrlG/sofnDv8kAbzKOD5pIPeUiI5VQY61NuWH+cItDw=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.113.0"
  constraints = "0.113.0"
  hashes = [
    "h1:du6IqLZwZWJ0cD8KJE+2BXKyxPzlySDIbagX9wzYtlo=",
  ]
}
