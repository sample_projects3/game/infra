
resource "kubernetes_namespace_v1" "namespace" {
  for_each = { for namespace in var.namespaces : namespace => namespace }

  metadata {
    labels = {
      name = each.key
    }
    name = each.key
  }
}
