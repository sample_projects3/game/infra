locals {
  cicd_service_acc_name = "ci-cd"
}

resource "kubernetes_service_account_v1" "cicd" {
  metadata {
    name      = local.cicd_service_acc_name
    namespace = var.applications_namespace
  }
}

resource "kubernetes_secret_v1" "cicd" {
  metadata {
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account_v1.cicd.metadata[0].name
    }
    namespace = var.applications_namespace

    name = "${kubernetes_service_account_v1.cicd.metadata[0].name}-token"
  }

  type                           = "kubernetes.io/service-account-token"
  wait_for_service_account_token = true
}

resource "kubernetes_role_v1" "cicd" {
  metadata {
    generate_name = null
    name          = local.cicd_service_acc_name
    namespace     = var.applications_namespace
  }
  rule {
    api_groups     = [""]
    resource_names = []
    resources      = ["configmaps", "persistentvolumeclaims", "pods", "pods/exec", "pods/log", "pods/portforward", "secrets", "services"]
    verbs          = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups     = ["apps"]
    resource_names = []
    resources      = ["deployments", "replicasets", "statefulsets"]
    verbs          = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups     = ["autoscaling"]
    resource_names = []
    resources      = ["horizontalpodautoscalers"]
    verbs          = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups     = ["networking.k8s.io"]
    resource_names = []
    resources      = ["ingresses"]
    verbs          = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups     = ["monitoring.coreos.com"]
    resource_names = []
    resources      = ["servicemonitors"]
    verbs          = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups     = ["autoscaling.k8s.io"]
    resource_names = []
    resources      = ["verticalpodautoscalers"]
    verbs          = ["create", "delete", "get", "list", "patch", "update", "watch"]
  }
}

resource "kubernetes_role_binding_v1" "cicd" {
  metadata {
    generate_name = null
    name          = "${local.cicd_service_acc_name}-role-binding"
    namespace     = var.applications_namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_service_account_v1.cicd.metadata[0].name
  }
  subject {
    api_group = null
    kind      = "ServiceAccount"
    name      = kubernetes_service_account_v1.cicd.metadata[0].name
    namespace = var.applications_namespace
  }
}
