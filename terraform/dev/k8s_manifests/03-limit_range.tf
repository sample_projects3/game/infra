

resource "kubernetes_limit_range_v1" "import" {
  metadata {
    annotations   = {}
    generate_name = null
    labels        = {}
    name          = "resource-constraint"
    namespace     = var.applications_namespace
  }
  spec {
    limit {
      default = {
        cpu    = "50m"
        memory = "40Mi"
      }
      default_request = {
        cpu    = "25m"
        memory = "40Mi"
      }
      max = {
        cpu    = "1"
        memory = "1000Mi"
      }
      max_limit_request_ratio = {}
      min = {
        cpu    = "10m"
        memory = "1Mi"
      }
      type = "Container"
    }
  }
}
