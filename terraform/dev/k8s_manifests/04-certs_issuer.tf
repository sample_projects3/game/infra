locals {
  issuer_name = "letsencrypt"
}

resource "kubernetes_manifest" "issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "Issuer"
    metadata = {
      name      = local.issuer_name
      namespace = var.applications_namespace
    }
    spec = {
      acme = {
        preferredChain = ""
        privateKeySecretRef = {
          name = "${local.issuer_name}-secretref"
        }
        server        = var.issuer_server
        skipTLSVerify = var.issuer_skip_tls_verify
        solvers = [{
          http01 = {
            ingress = {
              class = "nginx"
            }
          }
        }]
      }
    }
  }
}
