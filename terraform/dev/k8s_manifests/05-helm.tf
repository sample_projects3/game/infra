
#  ---------------------------------------------------------------------------------
#  ingress
#  ---------------------------------------------------------------------------------

locals {
  ingress_chart_path = "${var.helm_charts_path}/ingress-nginx"
  # force terraform to show diffs in a local chart.
  ingress_chart_hash = md5(join("", [
    for file in fileset(local.ingress_chart_path, "**") :
    filemd5(format("%s/%s", local.ingress_chart_path, file))
  ]))

  ingress_chart_namespace = "ingress"
  ingress_chart_name      = "ingress-nginx"
}

resource "helm_release" "nginx_ingress" {
  name      = local.ingress_chart_name
  chart     = local.ingress_chart_path
  namespace = local.ingress_chart_namespace

  timeout = 180

  values = [
    file("${local.ingress_chart_path}/values.${var.k8s_cluster}.yaml"),
    yamlencode({
      chart_hash : local.ingress_chart_hash,
    }),
  ]

  set {
    name  = "config.clusterIp"
    value = data.yandex_vpc_address.nlb.external_ipv4_address[0].address
  }

  wait          = true
  wait_for_jobs = true
  atomic        = true

  depends_on = [kubernetes_namespace_v1.namespace]
}

# for showing helm diff in tf outputs
data "helm_template" "nginx_ingress" {
  name      = local.ingress_chart_name
  chart     = local.ingress_chart_path
  namespace = local.ingress_chart_namespace

  values = [
    file("${local.ingress_chart_path}/values.${var.k8s_cluster}.yaml"),
    yamlencode({
      chart_hash : local.ingress_chart_hash,
    }),
  ]

  set {
    name  = "config.clusterIp"
    value = data.yandex_vpc_address.nlb.external_ipv4_address[0].address
  }
}
