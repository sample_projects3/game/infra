data "yandex_resourcemanager_cloud" "this" {
  name = var.cloud_name
}

data "yandex_resourcemanager_folder" "this" {
  name     = var.folder_name
  cloud_id = data.yandex_resourcemanager_cloud.this.id
}

data "yandex_lockbox_secret" "terraform" {
  name      = var.terraform_token_secret_name
  folder_id = data.yandex_resourcemanager_folder.this.id
}

data "yandex_lockbox_secret_version" "terraform" {
  secret_id = data.yandex_lockbox_secret.terraform.id
}

data "yandex_vpc_address" "nlb" {
  name      = "nlb-k8s"
  folder_id = data.yandex_resourcemanager_folder.this.id
}
