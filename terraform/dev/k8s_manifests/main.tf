
provider "helm" {
  kubernetes {
    insecure = true
    token    = lookup({ for entry in data.yandex_lockbox_secret_version.terraform.entries : entry.key => entry.text_value }, "token", null)
    host     = data.terraform_remote_state.k8s.outputs.k8s_endpoint
  }
}

provider "kubernetes" {
  insecure = true
  token    = lookup({ for entry in data.yandex_lockbox_secret_version.terraform.entries : entry.key => entry.text_value }, "token", null)
  host     = data.terraform_remote_state.k8s.outputs.k8s_endpoint
}

provider "yandex" {
  service_account_key_file = "./../../credentials.json"
}

terraform {
  backend "s3" {}
}

data "terraform_remote_state" "k8s" {
  backend = "s3"
  config = {
    bucket                   = "n98gt-tf-state-game"
    key                      = "game-project/yandex/folders/${var.folder_name}/k8s/terraform.tfstate"
    region                   = "us-east-1"
    profile                  = "tf-state-acc"
    shared_credentials_files = ["./../../credentials"]
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true # required for tf v1.6.1+
  }
}
