# for showing diff in helm templates

output "ingress_manifest" {
  value = data.helm_template.nginx_ingress.manifest
}
