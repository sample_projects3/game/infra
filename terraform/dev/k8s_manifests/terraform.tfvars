cloud_name  = "company"
folder_name = "dev"

namespaces = ["ingress", "dev"]

k8s_cluster            = "dev"
applications_namespace = "dev"
