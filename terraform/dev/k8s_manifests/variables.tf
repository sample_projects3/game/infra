variable "folder_name" {
  description = "Name of the yandex folder where k8s cluster is located (list available folders: yc resource-manager folder list)"
  type        = string
}

variable "cloud_name" {
  description = "Name of the yandex folder where k8s cluster is located (list available clouds: yc resource-manager cloud list)"
  type        = string
}

variable "applications_namespace" {
  description = "Name of a namespace where apps are running"
  type        = string
}

variable "terraform_token_secret_name" {
  description = "Name of a yandex lockbox secret for terraform k8s token"
  type        = string
  default     = "k8s-terraform-acc-token"
}

variable "namespaces" {
  description = "List of namespaces that need to be created in k8s"
  type        = list(string)
  default     = []
}

variable "k8s_cluster" {
  description = <<EOT
  Name of a k8s cluster. Used to load cluster specific helm values files

  E.g.
  if a cluster name is «dev». helm chart will use file values.dev.yaml
  EOT
  type        = string
  default     = ""
}

variable "helm_charts_path" {
  type    = string
  default = "./../../../helm"
}


#  ---------------------------------------------------------------------------------
#  certificates issuer
#  ---------------------------------------------------------------------------------

variable "issuer_server" {
  description = "url used to access the ACME server's 'directory' endpoint"
  type        = string
  default     = "https://acme-v02.api.letsencrypt.org/directory"
}

variable "issuer_skip_tls_verify" {
  description = "Disable validation of the ACME server TLS certificate, i.e. allow insecure connection"
  type        = bool
  default     = false
}
