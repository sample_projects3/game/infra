terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.13.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.27.0"
    }
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "= 0.113.0"
    }
  }
  required_version = ">= 1.7.4"
}
