module "vpc" {
  source    = "github.com/terraform-yc-modules/terraform-yc-vpc.git?ref=bf916d3a4fcb6686065f9dcbe52a95f55197b096"
  folder_id = data.yandex_resourcemanager_folder.this.id

  network_name  = var.network_name
  create_nat_gw = true
  create_sg     = false
  private_subnets = [
    {
      name           = "default-ru-central1-a"
      zone           = "ru-central1-a"
      v4_cidr_blocks = ["10.128.0.0/24"]
    },
    {
      name           = "default-ru-central1-b"
      zone           = "ru-central1-b"
      v4_cidr_blocks = ["10.129.0.0/24"]
    },
    {
      name           = "default-ru-central1-d"
      zone           = "ru-central1-d"
      v4_cidr_blocks = ["10.130.0.0/24"]
    }
  ]
}
