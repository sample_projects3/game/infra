resource "yandex_vpc_address" "nlb" {
  name      = "nlb-k8s"
  folder_id = data.yandex_resourcemanager_folder.this.id

  external_ipv4_address {
    zone_id = "ru-central1-b"
  }
}
