output "network_id" {
  value = module.vpc.vpc_id
}

output "private_subnets" {
  value = module.vpc.private_subnets
}

output "nlb_ip" {
  value = yandex_vpc_address.nlb
}
