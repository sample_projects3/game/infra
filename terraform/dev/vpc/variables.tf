variable "network_name" {
  type    = string
  default = "default"
}

variable "folder_name" {
  type = string
}

variable "cloud_name" {
  type = string
}
