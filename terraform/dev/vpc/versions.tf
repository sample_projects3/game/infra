terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "= 0.113.0"
    }
  }
  required_version = ">= 1.7.4"
}
