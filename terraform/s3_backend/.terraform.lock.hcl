# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "5.2.0"
  constraints = "> 5.1.0, 5.2.0"
  hashes = [
    "h1:91GuJfDcJfijg/bdX77ckr6iLz5Pmmehsk7SHzBmUQw=",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.6.0"
  constraints = "> 3.5.0"
  hashes = [
    "h1:R5Ucn26riKIEijcsiOMBR3uOAjuOMfI1x7XvH4P6B1w=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version     = "0.113.0"
  constraints = "> 0.9.0, 0.113.0"
  hashes = [
    "h1:du6IqLZwZWJ0cD8KJE+2BXKyxPzlySDIbagX9wzYtlo=",
  ]
}
