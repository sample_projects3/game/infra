resource "yandex_kms_symmetric_key" "service_accs_creds" {
  name              = var.kms_key_name
  folder_id         = data.yandex_resourcemanager_folder.this.id
  description       = "Key for encrypting terraform service acc creds"
  default_algorithm = "AES_128"
}
