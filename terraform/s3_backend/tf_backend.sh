#!/usr/bin/env bash

# This script:
#  - generates terraform remote state backend settings
# Notes:
# - aws cli tool must be installed
# - yandex-cloud cli tool must be installed and configured/authorized

set -o errexit #(set -e)
set -o nounset #(set -u)
set -o pipefail
# set -o xtrace #(set -x)
# set -o errtrace #(set -E) # inherit the ERR trap in subshell and functions
set -u #fail on unset vars

main(){
    generate_tf_state_settings
}

generate_tf_state_settings(){
    local yandex_folder_name=""

    local yandex_folders_list
    yandex_folders_list=$(yc resource-manager folder list)
    readonly yandex_folders_list

    echo " --- "
    while [[ -z "${yandex_folder_name// }" ]];do
        # shellcheck disable=SC2162
        read -ep "Enter name of yandex folder to use: "$'\n'"$yandex_folders_list "$'\n'"Yandex folder name: " -i "default" yandex_folder_name
    done
    readonly yandex_folder_name

    local yandex_folder_id
    yandex_folder_id=$(yc resource-manager folder get --name "${yandex_folder_name}" | grep -oP "(?<=^id: ).*")
    readonly yandex_folder_id

    local yandex_cloud_id
    yandex_cloud_id=$(yc resource-manager folder get --name "${yandex_folder_name}" | grep -oP "(?<=^cloud_id: ).*")
    readonly yandex_cloud_id

    export _YANDEX_FOLDER_ID="${yandex_folder_id}"
    export _YANDEX_CLOUD_ID="${yandex_cloud_id}"

    local tf_service_acc_name
    read -rep "Enter a name of a new service acc: " -i "terraform-default" tf_service_acc_name;
    readonly tf_service_acc_name

    local tf_service_acc_output
    tf_service_acc_output=$(yc iam service-account create --name "${tf_service_acc_name}" --folder-id "${_YANDEX_FOLDER_ID}" --description "terraform acc for managing cloud resources")
    readonly tf_service_acc_output


    local tf_service_acc_id
    tf_service_acc_id=$(echo -n "${tf_service_acc_output}" | grep -oP "(?<=^id: ).*" )

    export _TERRAFORM_SERVICE_ACC_ID="${tf_service_acc_id}"

    echo " ===> assigning admin role to ${tf_service_acc_name} service acc …"
    # yc resource-manager folder add-access-binding "${_YANDEX_FOLDER_ID}" --role admin --subject serviceAccount:"${_TERRAFORM_SERVICE_ACC_ID}"
    yc resource-manager cloud add-access-binding "${_YANDEX_CLOUD_ID}" --role admin --subject "serviceAccount:${_TERRAFORM_SERVICE_ACC_ID}"

    echo " ===> creating authorization key for ${tf_service_acc_name} service acc …"
    yc iam key create \
        --service-account-id "${_TERRAFORM_SERVICE_ACC_ID}" \
        --folder-id "${_YANDEX_FOLDER_ID} "\
        --description 'for terraform cli' \
        --output "$(git rev-parse --show-toplevel)/terraform/credentials.json"

    echo " --- "

    local tf_tfstate_service_acc_name
    read -rep "Enter a name of a new service acc for tf state: " -i "terraform-tfstate-default" tf_tfstate_service_acc_name;
    readonly tf_tfstate_service_acc_name

    local tf_tfstate_service_acc_output
    tf_tfstate_service_acc_output=$(yc iam service-account create --name "${tf_tfstate_service_acc_name}" --folder-id "${_YANDEX_FOLDER_ID}" --description "terraform acc to access tf-state in s3 storage")
    readonly tf_tfstate_service_acc_output

    local tf_tfstate_service_acc_id
    tf_tfstate_service_acc_id=$(echo -n "${tf_tfstate_service_acc_output}" | grep -oP "(?<=^id: ).*" )

    export _TERRAFORM_TFSTATE_SERVICE_ACC_ID="${tf_tfstate_service_acc_id}"

    echo " ===> assigning storage.editor and ydb.editor roles to ${tf_tfstate_service_acc_name} service acc …"
    yc resource-manager folder add-access-binding "${_YANDEX_FOLDER_ID}" --role storage.editor --subject serviceAccount:"${_TERRAFORM_TFSTATE_SERVICE_ACC_ID}"
    yc resource-manager folder add-access-binding "${_YANDEX_FOLDER_ID}" --role ydb.editor --subject serviceAccount:"${_TERRAFORM_TFSTATE_SERVICE_ACC_ID}"

    echo " ===> creating static access key for terraform-tfstate acc to access tfstate files in s3 storage …"
    local tfstate_service_acc_static_key
    tfstate_service_acc_static_key=$(yc iam access-key create --service-account-id "${_TERRAFORM_TFSTATE_SERVICE_ACC_ID}" --description 'access to tfstate in s3 storage and tfstate locks in yandex db')
    readonly tfstate_service_acc_static_key

    local tf_tfstate_service_acc_key_id
    tf_tfstate_service_acc_key_id=$(echo "${tfstate_service_acc_static_key}" | grep -oP "(?<=key_id: ).*" )
    readonly tf_tfstate_service_acc_key_id

    local tf_tfstate_service_acc_access_key
    tf_tfstate_service_acc_access_key=$(echo "${tfstate_service_acc_static_key}" | grep -oP "(?<=^secret: ).*" )
    readonly tf_tfstate_service_acc_access_key

    echo " ===> create credentials file for aws cli …"
    cat  <<EOF > "$(git rev-parse --show-toplevel)/terraform/credentials"
; for access to tfstates in s3 storage and tfstate locking in dynamo-db
[tf-state-acc]
aws_access_key_id=${tf_tfstate_service_acc_key_id}
aws_secret_access_key=${tf_tfstate_service_acc_access_key}
EOF

    chmod 600 "$(git rev-parse --show-toplevel)/terraform/credentials"

    echo " ===> create temporary tf_backend_override.tf file with local tf backend"
    cd "$(git rev-parse --show-toplevel)/terraform/s3_backend"
    cat <<EOF > tf_backend_override.tf
terraform {
backend "local" {}
}
EOF

    local yandex_cloud_name
    yandex_cloud_name=$(yc resource-manager cloud get --id "${yandex_cloud_id}" | grep -oP "(?<=^name: ).*")
    readonly yandex_cloud_name

    local s3_bucket_name
    # shellcheck disable=SC2162
    read -ep "enter s3 bucket name for tf state (E.g. uniq-tf-state-name): " -i "${yandex_cloud_name}-tf-state" s3_bucket_name;
    readonly s3_bucket_name

    local tf_state_lock_database_name
    # shellcheck disable=SC2162
    read -ep "enter Name of ydb database name for terraform state locking: " -i "terraform-tfstate-locks" tf_state_lock_database_name;
    readonly tf_state_lock_database_name

    local kms_key_name
    # shellcheck disable=SC2162
    read -ep "enter name of kms key for encrypting terraform creds files (E.g. tf-creds-symmetric-key): " -i "tf-creds-symmetric-key" kms_key_name;
    readonly kms_key_name

    sed -E -i 's/(bucket\s*=\s*")[^"]*(")/\1'"${s3_bucket_name}"'\2/g' "$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend"
    sed -E -i 's/(bucket_name\s*=\s*")[^"]*(")/\1'"${s3_bucket_name}"'\2/g' "$(git rev-parse --show-toplevel)/terraform/s3_backend/terraform.tfvars"
    sed -E -i 's/(folder_id\s*=\s*")[^"]*(")/\1'"${yandex_folder_id}"'\2/g' "$(git rev-parse --show-toplevel)/terraform/s3_backend/terraform.tfvars"
    sed -E -i 's/(cloud_id\s*=\s*")[^"]*(")/\1'"${yandex_cloud_id}"'\2/g' "$(git rev-parse --show-toplevel)/terraform/s3_backend/terraform.tfvars"
    sed -E -i 's/(tf_state_lock_database_name\s*=\s*")[^"]*(")/\1'"${tf_state_lock_database_name}"'\2/g' "$(git rev-parse --show-toplevel)/terraform/s3_backend/terraform.tfvars"
    sed -E -i 's/(kms_key_name\s*=\s*")[^"]*(")/\1'"${kms_key_name}"'\2/g' "$(git rev-parse --show-toplevel)/terraform/s3_backend/terraform.tfvars"

    echo " ===> installing tf dependencies … "
    terraform init
    echo " ===> creating tf plan … "
    terraform plan -out=tfplan
    # shellcheck disable=SC2162
    read -ep "apply terraform plan (yes/no)? " -i "yes" apply_terraform_plan;
    readonly apply_terraform_plan

    if [[ $apply_terraform_plan != "yes" ]]; then
        echo "apply_terraform_plan is skipped, aborting script execution"
        return
    fi
    echo " ===> applying tf plan … "
    terraform apply tfplan && rm tfplan

    echo " ===> getting ydb endpoint …"
    local ydb_endpoint
    ydb_endpoint=$(yc ydb database get "${tf_state_lock_database_name}" --folder-id "${yandex_folder_id}" |grep -oP "(?<=^document_api_endpoint: ).*")
    readonly ydb_endpoint

    echo " ===> saving ydb endpoint …"
    sed -E -i 's|(dynamodb\s*=\s*")[^"]*(")|\1'"${ydb_endpoint}"'\2|g' "$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend"

    AWS_SHARED_CREDENTIALS_FILE="$(git rev-parse --show-toplevel)/terraform/credentials"
    export AWS_SHARED_CREDENTIALS_FILE

    echo " ===> creating ydb tf_locks table … "
    aws dynamodb create-table \
    --table-name tf_locks \
    --attribute-definitions \
        AttributeName=LockID,AttributeType=S \
    --key-schema \
        AttributeName=LockID,KeyType=HASH \
    --endpoint "${ydb_endpoint}" \
    --profile tf-state-acc \
    --region ru-central1

    echo " ===> transfering local tf state to s3 bucket … "
    rm -f tf_backend_override.tf
    terraform init -backend-config="$(git rev-parse --show-toplevel)/terraform/s3_backend_common.tfbackend" -backend-config=s3_backend.tfbackend -migrate-state && \
    rm terraform.tfstate

    echo " ===> encrypting yandex IAM key for terraform service acc … "
    yc kms symmetric-crypto encrypt \
        --id "$(yc kms symmetric-key get ${kms_key_name} --folder-id "${yandex_folder_id}" | grep -oP "(?<=^id: ).*")" \
        --plaintext-file "$(git rev-parse --show-toplevel)/terraform/credentials.json" \
        --ciphertext-file "$(git rev-parse --show-toplevel)/terraform/credentials.json.encrypted"

    echo " ===> encrypting access key for terraform tf-state service acc … "
    yc kms symmetric-crypto encrypt \
        --id "$(yc kms symmetric-key get ${kms_key_name} --folder-id "${yandex_folder_id}" | grep -oP "(?<=^id: ).*")" \
        --plaintext-file "$(git rev-parse --show-toplevel)/terraform/credentials" \
        --ciphertext-file "$(git rev-parse --show-toplevel)/terraform/credentials.encrypted"
}

main "$@"
