module "s3_bucket_tfstate" {
  source = "github.com/terraform-yc-modules/terraform-yc-s3.git?ref=1.0.2"

  bucket_name   = var.bucket_name
  force_destroy = false # prevent bucket deletion if it is not empty
  folder_id     = data.yandex_resourcemanager_folder.this.id

  versioning = {
    enabled = true
  }

  lifecycle_rule = [
    {
      enabled = true
      id      = "delete old tfstate versions after ${var.old_tf_state_versions_deletion_period} days" # i.e. rule description
      prefix  = ""                                                                                    # empty string — for all objects
      noncurrent_version_expiration = {
        days = var.old_tf_state_versions_deletion_period
      }
    },
  ]

}
