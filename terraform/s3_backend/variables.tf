variable "cloud_id" {
  type = string
}

variable "folder_name" {
  description = "Name of a folder where tf remote state will be stored"
  type        = string
  default     = "default"
}

variable "aws_profile_for_s3_backend" {
  description = <<EOT
  Profile name from aws credentials file
  Must be equal to «profile» value in s3_backend_common.tfbackend
  EOT

  type    = string
  default = "tf-state-acc"
}

variable "tf_state_lock_database_name" {
  description = "Name of ydb database name for terraform state locking"
  type        = string
  default     = "terraform-tfstate-locks"
}

variable "bucket_name" {
  description = <<EOT
  s3 storage bucket name
  Must be unique across all yandex accs
  Must be equal to «bucket» value in s3_backend_common.tfbackend
  EOT

  type    = string
  default = ""
}

variable "kms_key_name" {
  description = "Name of KMS key for encrypting terraform service acc creds"
  type        = string
  default     = "tf-creds-symmetric-key"
}

variable "old_tf_state_versions_deletion_period" {
  description = "Delete old tf state versions in s3 bucket after X days"
  type        = number
  default     = 14
}
