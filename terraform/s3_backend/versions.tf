terraform {
  required_version = ">= 1.7.4"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.113.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "5.2.0"
    }
  }
}
